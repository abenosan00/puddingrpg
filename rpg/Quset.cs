using rpg;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.RegularExpressions;
// クエスト一覧
class Quest
{
    GameStart gameStart = new GameStart();
    OpeMonster opeMonster = new OpeMonster();

    IDictionary<int, string> quest = new Dictionary<int, string>()
    {
        { 1, "はじまりの道 / 1〜10Lv" },
        { 2, "森の中 / 11〜21Lv" },
        { 3, "明かりのない世界" },
        { 4, "試練の回廊" },
        { 5, "ゴブリンの穴" },
        { 6, "中央公園" },
        { 7, "物盗りのアジト" },
        { 8, "ひよの森" },
        { 9, "もさもさ洞窟" },
        { 10, "イラク戦争" },
        { 11, "キャラメル王国" },
        { 12, "ぷりん大国(改)" },
    };

    // ダンジョンリストを表示する
    public int DisplayQuestList()
    {
        Console.WriteLine("挑戦するダンジョンを数値で選択してください。");
        Console.WriteLine("■■■■■ダンジョン一覧■■■■■");
        foreach (var questList in quest)
        {
            Console.WriteLine("[" + questList.Key + "]" + ":" + questList.Value);
        }
        Console.WriteLine("■■■■■■■■■■■■■■■■■");

        Console.WriteLine("ダンジョンを選択してください。");

        string selectNum = Regex.Replace(Console.ReadLine(), @"[^0-9]", "");
        if (!String.IsNullOrEmpty(selectNum))
        {
            var quetsNum = int.Parse(selectNum);
            return quetsNum;
        }
        else
        {
            return 0;
        }
    }

    // ダンジョンに参加
    public void AdmissionQuest(Character character)
    {
        foreach (var questNum in quest)
        {
            if(character.questMode == questNum.Key)
            {
                StartQuest(character, questNum.Key, questNum.Value);
            }
        }
    }

    // ダンジョンスタート
    public bool StartQuest(Character character, int questKey, string questName)
    {
        var clearFlg = true;
        Random dice = new Random();
        // クリアマス
        const int CLEAR_NUM = 100;
        // 現在のマス
        var currentNum = 0;

        Console.WriteLine("");
        Console.WriteLine("");
        Console.WriteLine("");
        Console.WriteLine($"■■■■■{questName}■■■■■");
        while (clearFlg && currentNum <= CLEAR_NUM)
        {
            Console.WriteLine("[1]:サイコロを振る / [2]:ダンジョンを抜ける");
            var diceNum = Regex.Replace(Console.ReadLine(), @"[^0-9]", "");
            if (!String.IsNullOrEmpty(diceNum))
            {
                switch (int.Parse(diceNum))
                {
                    case 1:
                        var nextDice = dice.Next(6) + 1;
                        // サイコロの目が 1, 3, 6 の場合モンスター出現
                        if (nextDice == 1 || nextDice == 3 || nextDice == 6)
                        {
                            Combat(character, questKey);
                        }
                        currentNum += nextDice;
                        Console.WriteLine($"{ nextDice } マス進んだ。ゴールまで残り{ CLEAR_NUM - currentNum }");
                        break;

                    case 2:
                        gameStart.Start(character);
                        break;

                    default:
                        Console.WriteLine("半角数値で[1]:サイコロを振る / [2]:ダンジョンを抜ける を選択してください。");
                        break;
                }
            }
        }

        if (clearFlg)
        {
            var getExp = dice.Next(character.level + 30);
            var getMoney = dice.Next(character.level + 30);
            Console.WriteLine("ダンジョンをクリアしました！！");
            Console.WriteLine($"{character.name }は経験値 { getExp }を手にした。");
            Console.WriteLine($"{character.name }はプリン銭 { getMoney }銭を手にした。");
            character.exp += getExp;
            character.money += getMoney;
        }
        return clearFlg;
    }

    // 戦闘
    public void Combat(Character character, int questKey)
    {
        var clearFlg = true;
        var monster = opeMonster.displayMonster(questKey);
        Console.WriteLine("");
        Console.WriteLine("");
        Console.WriteLine("--------------------------------");
        Console.WriteLine($"{monster[0].name} が現れた！");
        Console.WriteLine("--------------------------------");
        while (clearFlg)
        {
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("[1]:攻撃 / [2]:回復 /[3]:逃げる");
            Console.WriteLine($"■■■■{ character.name }■■■■");
            Console.WriteLine($"■ Level : { character.level } / HP : { character.hp } / MP : { character.mp }");
            Console.WriteLine("--------------------------------");
            string diceNum = Regex.Replace(Console.ReadLine(), @"[^0-9]", "");
            if (!String.IsNullOrEmpty(diceNum))
            {
                switch (int.Parse(diceNum))
                {
                    case 1:
                        if (character.hp >= 0)
                        {
                            Console.WriteLine("------------戦闘------------");
                            var atk = character.Attack();

                            Console.WriteLine($"{ character.name }の攻撃！！！");
                            Console.WriteLine($"{ character.name }が[ { monster[0].name }] に[ " + atk + " ]のダメージを与えた。");
                            monster[0].hp -= atk;
                            if (monster[0].hp <= 0)
                            {
                                // モンスター死亡
                                clearFlg = false;
                                Console.WriteLine($"[ { monster[0].name }] を倒した。");
                                Console.WriteLine($"{ character.name } は[ { monster[0].exp }] 経験値を手にした。");
                                character.exp += monster[0].exp;
                                LeveTable.CharLevel(character);
                                monster.Clear();
                                Console.WriteLine("------------戦闘終了------------");
                            }
                            else
                            {
                                Console.WriteLine($"{ monster[0].name }が攻撃してきた!! [ " + atk + " ]のダメージを食らった。");
                                character.hp -= atk;
                                Console.WriteLine("------------戦闘------------");
                            }

                        }
                        break;

                    case 2:
                        Console.WriteLine("未実装だった。。。");
                        break;

                    case 3:
                        Console.WriteLine($"{ character.name }は戦闘から逃げた。");
                        gameStart.Start(character);
                        break;

                    default:
                        Console.WriteLine("半角数値で[1]:攻撃 / [2]:回復 / [3]:逃げる を選択してください。");
                        break;
                }
            }
        }
    }
}
