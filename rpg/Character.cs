﻿using System;
using System.Collections.Generic;

// ヒーロステータス
class Character
{
    Random rad = new Random();
    public string name;
    public int level = 1;
    public int exp = 0;
    public int str = 1;
    public int hp = 100;
    public int mp = 10;
    public int money = 0;
    public int questMode = 1;


    public Character(string name)
    {
        this.name = name;
    }

    public int Attack()
    {
        Random rad = new Random();
        return rad.Next(6) + str;
    }

    public int Heel()
    {
        return rad.Next(6);
    }

    // TODO: スキル番号を選択してもらい、発動スキルを選択する
    public int Skill()
    {
        return rad.Next(500) + level;
    }

    // ステータスの確認
    public void ConfStatus()
    {
        Console.WriteLine("■■■■■ステータス■■■■■");
        Console.WriteLine($"名前     : { this.name }");
        Console.WriteLine($"レベル   : { this.level }");
        Console.WriteLine($"HP / MP  : { this.hp } / { this.mp }");
        Console.WriteLine($"経験値   : { this.exp }");
        Console.WriteLine($"攻撃力   : { this.str }");
        Console.WriteLine($"ぷりんｾﾞﾆ: { this.money }");
        Console.WriteLine("■■■■■■■■■■■■■■■");
    }
    //public int Skill(int skillNum) {
    //    return rad.Next(500) + level;
    //}
}

class Monster
{
    public int questMode;
    public string name;
    public int level;
    public int exp;
    public int str;
    public int hp;
    public int money;
}

// キャラクターのレベルテーブル
class LeveTable
{
    private static int[] exp_table = new int[49]
    {
            10, 21, 33, 46, 60, 75, 92, 111, 132, 155,   // 1~10
            180, 208, 239, 273, 310, 351, 396, 446, 501,562,
            629, 703, 784, 873, 971, 1079, 1198, 1329, 1473, 1631,
            1805, 1996, 2206, 2437, 2691, 2970, 3277, 3615, 3987, 4396,
            4846, 5341, 5886, 6486, 7146, 7872, 8671, 9550, 10517
    };
    public static void CharLevel(Character character)
    {
        if (character.exp >= exp_table[character.level - 1])
        {
            character.level++;
            Console.WriteLine($"{ character.name }のレベルが [ {character.level - 1 } ]から [{ character.level }]にアップした。");
        }
    }
}

class OpeMonster
{
    List<Monster> monsterList = new List<Monster>();
    Random rad = new Random();
    // 指定されたモンスターを出現
    public List<Monster> displayMonster(int questMode) {
        var monsterNum = rad.Next(2) + 1;
        switch (questMode) {
            case 1:
                questMonster1(monsterNum);
                break;
            case 2:
                questMonster2(monsterNum);
                break;
            case 3:
                questMonster3(monsterNum);
                break;
            case 4:
                questMonster4(monsterNum);
                break;
            case 5:
                questMonster5(monsterNum);
                break;
            case 6:
                questMonster6(monsterNum);
                break;
        }


        return monsterList;
    }

    private List<Monster> questMonster1(int monsterNum)
    {
        switch (monsterNum)
        {
            case 1:
                monsterList.Add(new Monster() { name = "スライム", level = 2, exp = 2, hp = 6, money = 2, str = rad.Next(2) + 1 });
                break;
            case 2:
                monsterList.Add(new Monster() { name = "ゾンビ", level = 5, exp = 5, hp = 7, money = 5, str = rad.Next(2, 5) + 1 });
                break;
            case 3:
                monsterList.Add(new Monster() { name = "ゾンビ戦士", level = 7, exp = 7, hp = 8, money = 7, str = rad.Next(3, 7) + 1 });
                break;
        }
        return monsterList;
    }

    /// <summary>
    /// レベル帯 12～18
    /// </summary>
    /// <param name="monsterNum"></param>
    /// <returns></returns>
    private List<Monster> questMonster2(int monsterNum)
    {
        switch (monsterNum)
        {
            case 1:
                monsterList.Add(new Monster() { name = "50BTC保有者", level = 12, exp = 12, hp = 12, money = 2, str = rad.Next(3,12) + 1 });
                break;
            case 2:
                monsterList.Add(new Monster() { name = "XRP信者", level = 16, exp = 16, hp = 16, money = 5, str = rad.Next(5, 16) + 1 });
                break;
            case 3:
                monsterList.Add(new Monster() { name = "BTC元祖", level = 18, exp = 18, hp = 18, money = 7, str = rad.Next(7, 17) + 1 });
                break;
        }
        return monsterList;
    }

    private List<Monster> questMonster3(int monsterNum)
    {
        switch (monsterNum)
        {
            case 1:
                monsterList.Add(new Monster() { name = "街の番長", level = 24, exp = 35, hp = 32, money = 2, str = rad.Next(3, 12) + 1 });
                break;
            case 2:
                monsterList.Add(new Monster() { name = "お親分", level = 24, exp = 35, hp = 32, money = 2, str = rad.Next(3, 12) + 1 });
                break;
            case 3:
                monsterList.Add(new Monster() { name = "姉御", level = 24, exp = 35, hp = 32, money = 2, str = rad.Next(3, 12) + 1 });
                break;
        }
        return monsterList;
    }

    private List<Monster> questMonster4(int monsterNum)
    {
        switch (monsterNum)
        {
            case 1:
                monsterList.Add(new Monster() { name = "先生", level = 24, exp = 125, hp = 63, money = 2, str = rad.Next(3, 32) + 1 });
                break;
            case 2:
                monsterList.Add(new Monster() { name = "母親", level = 24, exp = 92, hp = 35, money = 2, str = rad.Next(3, 46) + 1 });
                break;
            case 3:
                monsterList.Add(new Monster() { name = "父親", level = 24, exp = 69, hp = 84, money = 2, str = rad.Next(3, 85) + 1 });
                break;
        }
        return monsterList;
    }

    private List<Monster> questMonster5(int monsterNum)
    {
        monsterList.Add(new Monster() { name = "スライム", level = 5, exp = 3, hp = 10, money = 5, str = 3 });
        monsterList.Add(new Monster() { name = "ゾンビ", level = 5, exp = 3, hp = 10, money = 5, str = 3 });
        monsterList.Add(new Monster() { name = "ゾンビ戦士", level = 5, exp = 3, hp = 10, money = 5, str = 3 });
        return monsterList;
    }

    private List<Monster> questMonster6(int monsterNum)
    {
        monsterList.Add(new Monster() { name = "スライム", level = 5, exp = 3, hp = 10, money = 5, str = 3 });
        monsterList.Add(new Monster() { name = "ゾンビ", level = 5, exp = 3, hp = 10, money = 5, str = 3 });
        monsterList.Add(new Monster() { name = "ゾンビ戦士", level = 5, exp = 3, hp = 10, money = 5, str = 3 });
        return monsterList;
    }


}