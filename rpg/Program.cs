﻿using System;
using System.Text.RegularExpressions;

namespace rpg
{
    class Program
    {
        static void Main(string[] args)
        {
            bool playFlg = true;
            Console.WriteLine("ぷりん大国START");
            Console.WriteLine("[1]:スタート [2]:終了");

            // ゲームPlay選択
            while (playFlg) {
                string selectNum = Regex.Replace(Console.ReadLine(), @"[^0-9]", "");
                if (!String.IsNullOrEmpty(selectNum))
                {
                    switch (int.Parse(selectNum))
                    {
                        case 1:
                            var gameStart = new GameStart();
                            Console.WriteLine("ヒーロの名前を決めてください。");
                            Console.Write("名前を入力してください  >>>  ");
                            var Character = new Character(Console.ReadLine());
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine($"{ Character.name }君！！今からダンジョンをスタートするよ！！");
                            Console.WriteLine("---------------------------------------------------");
                            Console.WriteLine("---------------------------------------------------");
                            gameStart.Start(Character);

                            Console.WriteLine("ゲームオーバー....");
                            break;

                        case 2:
                            Console.WriteLine("ゲームを終了します。");
                            playFlg = false;
                            break;

                        default:
                            Console.WriteLine("半角の [1] か [2] を入力してくだいさい。");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("半角の [1] か [2] を入力してくだいさい。");
                }
            }
        }
        //サイコロを振る
    }

    // ゲームスタート
    class GameStart {
        public const int CLEAR_POINT = 100;
        public int nowPoint = 0;

        public bool Start(Character character) {
            var Quest = new Quest();
            bool clearFlg = true;

            while (clearFlg) {
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("■■■■■■■■■■■■■■■■■■■■■■■■■");
                Console.WriteLine("[1]:ダンジョン選択 / [2]:ステータス / [3]:リタイア");
                Console.Write(">>>");
                string selectNum = Regex.Replace(Console.ReadLine(), @"[^0-9]", "");
                if (!String.IsNullOrEmpty(selectNum))
                {
                    switch (int.Parse(selectNum))
                    {
                        case 1:
                            character.questMode = Quest.DisplayQuestList();
                            if (character.questMode != 0)
                            {
                                Quest.AdmissionQuest(character);
                            }
                            break;

                        case 2:
                            character.ConfStatus();
                            break;

                        case 3:
                            clearFlg = false;
                            break;

                        default:
                            Console.WriteLine("----------------------------");
                            Console.WriteLine("半角数値で入力してください。");
                            Console.WriteLine("----------------------------");
                            break;
                    }
                }
            }
            return clearFlg;
        }
    }
}



